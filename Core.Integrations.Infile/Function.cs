using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Amazon.Lambda.Core;
using Amazon.Lambda.KinesisEvents;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using System.Linq;
using Core.Common;
using static Function;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

// Tutorial: Using AWS Lambda Aliases
// https://docs.aws.amazon.com/lambda/latest/dg/versioning-aliases-walkthrough1.html

// Pendings:
// 1. add persistent hashtable to store which payment_ids have already been invoiced
// 2. add persistent data store to have copy of all data in case infile fails
// 3. infer environment base on lambda alias
// 4. script to add event-source to all aliases or, inver environment based on event-source name (ex, loan-prod, loan-dev)
// 5. move initialize to Common

public class Function
{
  private static IConfiguration configuration = null;
  private static SlackNotifications slackNotifications = null;

  private ILambdaContext Context { get; set; }

  // Creaci�n de Objetos
  private ingface.dte dte = new ingface.dte();
  private ingface.detalleDte detalle = new ingface.detalleDte();
  //public ingface.requestDte registro = new ingface.requestDte();
  //public ingface.responseDte resultado = new ingface.responseDte();
  private static ingface.ingfaceClient ws = null;

  public void Initialize(KinesisEvent kinesisEvent, ILambdaContext context)
  {
    // check if services loaded globally
    System.Diagnostics.Debug.WriteLineIf(Function.configuration != null, "Global configuration already loaded");
    if (Function.configuration != null) return;

    string filePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "appsettings.dev.json");
    System.Diagnostics.Debug.Assert(File.Exists(filePath), $"Could not find configuration file", filePath);

    var alias = context?.InvokedFunctionArn?.Substring(context.InvokedFunctionArn.LastIndexOf(":") + 1);
    Context.Logger.LogLine($"ARN: {alias}, ALIAS: {alias}");

    string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

    Function.configuration = new ConfigurationBuilder()
     .SetBasePath(Directory.GetCurrentDirectory())
     .AddJsonFile("appsettings.dev.json", optional: false, reloadOnChange: true)
     .AddJsonFile($"appsettings.{alias}.json", optional: true, reloadOnChange: true)
     .AddJsonFile($"appsettings.{environment}.json", optional: true, reloadOnChange: true)
     .Build();

    Function.slackNotifications = new SlackNotifications();
    Function.ws = new ingface.ingfaceClient();
  }

  public object FunctionHandler(KinesisEvent kinesisEvent, ILambdaContext context)
  {
    this.Context = context;
    Initialize(kinesisEvent, context);


    var output = new List<object>();

    foreach (var record in kinesisEvent.Records)
    {
      string recordContent = GetRecordContents(record.Kinesis);
      context.Logger.LogLine(recordContent);

      var eventData = JObject.Parse(recordContent);
      if (Filter(eventData)) continue;

      var json = ParseFacturaJson(eventData["data"]);
      dte.BuildFactura(json, detalle, configuration);

      var result = GenerateInvoice(dte, json);
      output.Add(result);
    }

    context.Logger.LogLine(JsonConvert.SerializeObject(output, Formatting.Indented));

    var response = new
    {
      statusCode = 200,
      body = JsonConvert.SerializeObject(output),
      build = "1.0.1"
    };
    return response;

  }

  private bool Filter(JObject eventData)
  {
    if (eventData["eventType"]?.ToString() != "LoanLedger.AddEntry") return true;
    if (eventData["data"] == null) return true;
    if (eventData["data"]["category"]?.ToString() != "interest") return true;
    if (eventData["data"]["type"]?.ToString() != "payout" && eventData["data"]["type"]?.ToString() != "extension") return true;

    System.Diagnostics.Trace.Assert(eventData["data"]["user"] != null, "Missing 'user' property");
    System.Diagnostics.Trace.Assert(eventData["data"]["user"]["address"] != null, "Missing 'user.address' property");

    return false;
  }

  private object GenerateInvoice(ingface.dte _dte, ClientFactura json)
  {
    // Agregar Detalle a la Cabecera (DTE)
    //_dte.detalleDte[0] = _detalle;

    var registro = new ingface.requestDte();

    registro.dte = _dte;
    registro.usuario = configuration["AppSettings:Infile:usuario"];       // "VANATECH_PRU";
    registro.clave = configuration["AppSettings:Infile:clave"];           // "FC67998F31AB15E2B2963909ACEB0D62FC67998F31AB15E2B2963909ACEB0D62";
    var resultado = ws.registrarDteAsync(registro).Result.@return;

    // Comprobar resultado
    if (resultado.valido)
    {
      Console.WriteLine(resultado.cae);
      Console.WriteLine(resultado.numeroDte);

      //Context.Logger.LogLine($"face.cae = {resultado.cae}");
      //Context.Logger.LogLine($"face.numeroDte = {resultado.numeroDte}");
      return new
      {
        statusCode = 200,
        cae = resultado.cae,
        pdf = $"https://www.ingface.net/Ingfacereport/dtefactura.jsp?cae={resultado.cae}"
      };
    }
    else
    {
      System.Diagnostics.Debug.WriteLine("no es valido" + resultado.descripcion);
      //Context.Logger.LogLine($"error = {resultado.descripcion}");
      //throw new Exception(resultado.descripcion);
      
      slackNotifications.Notify(resultado.descripcion, json.user_id, json.payment_id, registro).Wait();
      throw new Exception(resultado.descripcion);
    }

  }


  #region Helpers

  public class ClientFactura
  {
    public DateTime created_at { get; set; }

    public string loan_id { get; set; }
    public string payment_id { get; set; }
    public string user_id { get; set; }

    public string user__fullname { get { return $"{this.user__first_name} {this.user__last_name}"; } }
    public string user__first_name { get; set; }
    public string user__last_name { get; set; }
    public string user__tax_id { get; set; }
    public string user__street { get; set; } // street
    public string user__phone { get; set; }
    public string user__email { get; set; }
    public string user__city { get; set; } // city
    public string user__state { get; set; } // state

    public string description { get; set; }

    public float subtotal { get; set; }
    public float vat_amount { get; set; }
  }

  private string GetDigits(string input)
  {
    try
    {
      return Regex.Replace(input, "[^0-9]", "");
    }
    catch (Exception ex)
    {
      return string.Empty;
    }
  }

  private string GetNIT(string input)
  {
    ValidateNIT(input, out string output);
    return output;
  }

  private bool ValidateNIT(string input, out string output)
  {
    const string empty_taxid = "C/F";
    output = empty_taxid;
    
    if (string.IsNullOrEmpty(input)) return false;

    var nit = input.Replace("-","").ToUpper(); ;
    var lastChar = nit.Length - 1;
    var number = nit.Substring(0, lastChar);
    var expectedCheker = nit.Substring(lastChar);

    var factor = number.Length + 1;
    var total = 0;

    for (var i = 0; i < number.Length; i++)
    {
      var character = number.Substring(i, 1);
      var digit = int.Parse(character);

      total += (digit * factor);
      factor = factor - 1;
    }

    var modulus = (11 - (total % 11)) % 11;
    var computedChecker = (modulus == 10 ? "K" : modulus.ToString());

    if (expectedCheker == computedChecker)
    {
      output = nit;
      return true;
    }

    return false;
  }

  private ClientFactura ParseFacturaJson(JToken data)
  {
    var factura = new ClientFactura();

    factura.created_at = DateTime.Parse((data["date"] ?? data["created_at"])?.ToString());     // DateTime.UtcNow.AddHours(-6).AddDays(-1);

    factura.user_id = data["user_id"]?.ToString();                           //"0P0USAAV3NXPsPHyeuhL5A3JFio2";
    factura.loan_id = data["loan_id"]?.ToString();                           //"ea554ed4-f49c-4a64-a959-954b02bd9356";
    factura.payment_id = data["payment_id"]?.ToString();                     //"-LTeMFn7jtzDeGgL-XCP";

    factura.user__first_name = data["user"]["first_name"]?.ToString();       //"MARIO VELAZCO";
    factura.user__last_name = data["user"]["last_name"]?.ToString();         //"MARIO VELAZCO";
    
    var inputNIT = data["user"]["tax_id"]?.ToString();                       //GetDigits("7656241-7");
    if (!ValidateNIT(inputNIT, out string outputNIT))
      Context?.Logger.LogLine($"Invalid value for 'user.tax_id', replacing '{inputNIT}' with '{outputNIT}'");
    factura.user__tax_id = outputNIT;       

    factura.user__phone = data["user"]["phone"]?.ToString();                 //"53120161";
    factura.user__email = data["user"]["email"]?.ToString();                 //"mario@vana.gt";
    factura.user__street = data["user"]["address"]["street"]?.ToString();    //"Ciudad";
    factura.user__city = data["user"]["address"]["city"]?.ToString();        //"guatemala";
    factura.user__state = data["user"]["address"]["state"]?.ToString();      //"guatemala";

    factura.description = "Intereses por pr�stamo otorgado.";
    factura.subtotal = float.Parse(data["subtotal"]?.ToString());            // 132.00;
    factura.vat_amount = float.Parse(data["vat_amount"]?.ToString());        // 18.00;

    return factura;
  }

  private string GetRecordContents(KinesisEvent.Record streamRecord)
  {
    using (var reader = new StreamReader(streamRecord.Data, Encoding.UTF8))
    {
      return reader.ReadToEnd();
    }
  }

  #endregion
}

public static class Extensions
{

  public static void BuildFactura(this ingface.dte dte, ClientFactura json, ingface.detalleDte detalle, IConfiguration configuration)
  {
    var resultado = new ingface.responseDte();

    int indice = 0;
    int cantidad_detalle = 1; // 2; // <-- cantidad de productos
    dte.detalleDte = new ingface.detalleDte[cantidad_detalle]; // Redimensi�n del tama�o del vector

    while (indice <= (cantidad_detalle - 1))
    {
      detalle = new ingface.detalleDte(); // Se inicializa el detalle para que acepte uno nuevo

      // Aqu� va el c�digo del detalle del punto 5.3.1 y 5.3.2

      // Asignaci�n de Par�metros | Detalle del Documento
      detalle.AddProducto(
        json,
        codigoProducto: "1",
        cantidad: 1
        );

      dte.SetCustomerDetails(json);
      dte.SetSellerDetails(configuration);

      // Asignaci�n de Par�metros | Cabecera del Documento
      dte.SetSerie(configuration);
      dte.SetMontos(json);
      dte.SetCabeceraPersonalizados(json);

      dte.detalleDte[indice] = detalle;
      indice++;
    }// Fin del ciclo
  }

  public static void SetSerie(this ingface.dte dte, IConfiguration configuration)
  {
    // Datos de Establecimientos Series y Dispositivos
    dte.codigoEstablecimiento = configuration["AppSettings:Infile:Serie:codigoEstablecimiento"];  //  "1";
    dte.idDispositivo = configuration["AppSettings:Infile:Serie:idDispositivo"];                  //  "001";
    dte.serieAutorizada = configuration["AppSettings:Infile:Serie:serieAutorizada"];              //  "FC";
    dte.numeroResolucion = configuration["AppSettings:Infile:Serie:numeroResolucion"];            //  "3161261220181";
    dte.fechaResolucion = DateTime.Parse(configuration["AppSettings:Infile:Serie:fechaResolucion"], new System.Globalization.CultureInfo("es-ES"));
    dte.fechaResolucionSpecified = bool.Parse(configuration["AppSettings:Infile:Serie:fechaResolucionSpecified"]);  //  true;
    dte.tipoDocumento = configuration["AppSettings:Infile:Serie:tipoDocumento"];                  //  "FACE";
    dte.serieDocumento = configuration["AppSettings:Infile:Serie:serieDocumento"];                //  "63";
    dte.nitGFACE = configuration["AppSettings:Infile:Serie:nitGFACE"];                            // "100634001";
  }

  public static void SetCustomerDetails(this ingface.dte dte, ClientFactura json)
  {
    // Datos de la Factura y el Comprador
    dte.numeroDocumento = "N/A";
    dte.fechaDocumento = json.created_at;                          //DateTime.Parse("26/12/2018", new System.Globalization.CultureInfo("es-ES"));                                                                 
                                                                   //dte.fechaAnulacion = DateTime.Parse("06/13/2018", new System.Globalization.CultureInfo("es-ES"));

    dte.nitComprador = json.user__tax_id;                          // "15240827";
    dte.nombreComercialComprador = json.user__fullname.ToUpper();  //"CARLOS LOPEZ";
    dte.direccionComercialComprador = json.user__street.ToUpper(); //"5 CALLE 2-22 Z.9";
    dte.telefonoComprador = json.user__phone;                      //"45669988";
    dte.correoComprador = json.user__email.ToUpper();              //"N/A";
    dte.municipioComprador = json.user__city.ToUpper();            //"GUATEMALA";
    dte.departamentoComprador = json.user__state.ToUpper();        //"GUATEMALA";

    dte.fechaDocumentoSpecified = true;
    dte.fechaAnulacionSpecified = true;
    dte.estadoDocumento = "ACTIVO";
    dte.codigoMoneda = "GTQ";
    dte.tipoCambio = 1.00;
    dte.tipoCambioSpecified = true;
    dte.regimen2989 = false;
  }

  public static void SetMontos(this ingface.dte dte, ClientFactura json)
  {
    // Valores o Montos
    dte.importeBruto = json.subtotal;                              //200.00;
    dte.detalleImpuestosIva = json.vat_amount;                     //24.00;
    dte.importeNetoGravado = json.subtotal + json.vat_amount;   //224.00;
    dte.montoTotalOperacion = json.subtotal + json.vat_amount;  //224;

    dte.importeDescuento = 0.00;
    dte.importeDescuentoSpecified = true;
    dte.importeTotalExento = 0.00;
    dte.importeTotalExentoSpecified = true;
    dte.importeOtrosImpuestos = 0.00;
    dte.importeOtrosImpuestosSpecified = true;
    dte.descripcionOtroImpuesto = "N/A";

    dte.importeBrutoSpecified = true;
    dte.detalleImpuestosIvaSpecified = true;
    dte.importeNetoGravadoSpecified = true;
  }

  public static void SetSellerDetails(this ingface.dte dte, IConfiguration configuration)
  {
    // Datos del Vendedor
    dte.nitVendedor = configuration["AppSettings:Infile:Vendedor:nitVendedor"];                                               //  "100634001";
    dte.nombreComercialRazonSocialVendedor = configuration["AppSettings:Infile:Vendedor:nombreComercialRazonSocialVendedor"]; //  "VANA TECH";
    dte.nombreCompletoVendedor = configuration["AppSettings:Infile:Vendedor:nombreCompletoVendedor"];                         //  "VANA TECH, SOCIEDAD ANONIMA";
    dte.direccionComercialVendedor = configuration["AppSettings:Infile:Vendedor:direccionComercialVendedor"];                 //  "Boulevard Los Proceres 24-69. Zona Pradera, Torre III, Oficina 512. Zona 10.";
    dte.municipioVendedor = configuration["AppSettings:Infile:Vendedor:municipioVendedor"];                                   //  "GUATEMALA";
    dte.departamentoVendedor = configuration["AppSettings:Infile:Vendedor:departamentoVendedor"];                             //  "GUATEMALA";
    dte.observaciones = configuration["AppSettings:Infile:Vendedor:observaciones"];                                           //  "N/A";
    dte.regimenISR = configuration["AppSettings:Infile:Vendedor:regimenISR"];                                                 //  "RET_DEFINITIVA";
  }

  public static void SetCabeceraPersonalizados(this ingface.dte dte, ClientFactura json)
  {
    // Campos personalizados
    dte.personalizado_01 = json.user_id;     //"N/A";
    dte.personalizado_02 = json.loan_id;     //"N/A";
    dte.personalizado_03 = json.payment_id;  //"N/A";
    dte.personalizado_04 = "N/A";
    dte.personalizado_05 = "N/A";
    dte.personalizado_06 = "N/A";
    dte.personalizado_07 = "N/A";
    dte.personalizado_08 = "N/A";
    dte.personalizado_09 = "N/A";
    dte.personalizado_10 = "N/A";
    dte.personalizado_11 = "N/A";
    dte.personalizado_12 = "N/A";
    dte.personalizado_13 = "N/A";
    dte.personalizado_14 = "N/A";
    dte.personalizado_15 = "N/A";
    dte.personalizado_16 = "N/A";
    dte.personalizado_17 = "N/A";
    dte.personalizado_18 = "N/A";
    dte.personalizado_19 = "N/A";
    dte.personalizado_20 = "N/A";
  }

  public static void AddProducto(this ingface.detalleDte detalle, ClientFactura json, string codigoProducto, float cantidad)
  {
    // Campos obligatorios
    detalle.cantidad = cantidad;
    detalle.codigoProducto = codigoProducto;
    detalle.cantidadSpecified = true;

    detalle.descripcionProducto = json.description;                     // "Producto 1";
    detalle.precioUnitario = json.subtotal + json.vat_amount;        // Convert.ToDouble("224.00");
    detalle.montoBruto = json.subtotal;                                 // Convert.ToDouble("200.00");
    detalle.detalleImpuestosIva = json.vat_amount;                      // Convert.ToDouble("24.00");
    detalle.importeNetoGravado = json.subtotal + json.vat_amount;    // Convert.ToDouble("224.00");
    detalle.importeTotalOperacion = json.subtotal + json.vat_amount; // Convert.ToDouble("224.00");

    detalle.precioUnitarioSpecified = true;
    detalle.montoBrutoSpecified = true;
    detalle.detalleImpuestosIvaSpecified = true;

    detalle.importeNetoGravadoSpecified = true;
    detalle.montoDescuento = Convert.ToDouble("0.00");
    detalle.montoDescuentoSpecified = true;
    detalle.importeExento = Convert.ToDouble("0.00");
    detalle.importeExentoSpecified = true;
    detalle.importeOtrosImpuestos = Convert.ToDouble("0.00");
    detalle.importeOtrosImpuestosSpecified = true;
    detalle.importeTotalOperacionSpecified = true;
    detalle.unidadMedida = "LBS";
    detalle.tipoProducto = "B";


    // Campos personalizados
    detalle.personalizado_01 = "N/A";
    detalle.personalizado_02 = "N/A";
    detalle.personalizado_03 = "N/A";
    detalle.personalizado_04 = "N/A";
    detalle.personalizado_05 = "N/A";
    detalle.personalizado_06 = "N/A";
  }

}